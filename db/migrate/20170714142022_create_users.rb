class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :first_name, :null => false
      t.string :last_name, :null => false
      t.integer :department_id, :null => false
      t.integer :position_id, :null => false
      t.integer :salary
      t.integer :state, :null => false
    end
  end
end
