class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.string :new_value
      t.string :old_value
      t.integer :type_id, :null => false
      t.integer :date, :null => false
      t.integer :user_id, :null => false
    end
  end
end
