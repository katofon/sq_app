class UsersController < ApplicationController

  USER_PARAMS_FOR_TRACK = {
    "department_id" => TransactionsHelper::DEPARTMENT_TYPE, 
    "position_id" => TransactionsHelper::POSITION_TYPE, 
    "salary" => TransactionsHelper::SALARY_TYPE
  }

  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  def show
  end

  # GET /users/new
  def new
    @user = User.new
    @user.state = UsersHelper::WORK_STATE
    @user.is_new = true
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.state = UsersHelper::WORK_STATE
    
    respond_to do |format|
      if @user.save
        Transaction.add_transaction(@user.id, TransactionsHelper::ACCEPT_JOB_TYPE)
        format.html { redirect_to @user, notice: 'Сотрудник успешно создан.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def fire
    @user = User.find(params["user_id"].to_i)
    respond_to do |format|
      if @user.present?
        @user.state = UsersHelper::FIRED_STATE
        @user.save
        Transaction.add_transaction(@user.id, TransactionsHelper::FIRE_JOB_TYPE, nil, params['reason'])
        format.html { redirect_to users_path, notice: 'Сотрудник уволен.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end

  end


  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    old_user = User.find(params["id"])

    USER_PARAMS_FOR_TRACK.each{|param_name, transaction_type_id| 
      old_val = old_user[param_name].to_s
      new_val = user_params[param_name].to_s
      Transaction.add_transaction(
        params["id"], 
        transaction_type_id, 
        nil, 
        Transaction.user_val_description(transaction_type_id, new_val), 
        Transaction.user_val_description(transaction_type_id, old_val)
      ) if new_val != old_val
    }
    
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'Изменения сохранены.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'Сотрудник удален.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :department_id, :position_id, :salary, :state)
    end
end
