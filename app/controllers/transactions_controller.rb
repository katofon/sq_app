class TransactionsController < ApplicationController

  # GET /transactions
  # GET /transactions.json

  # GET /transactions/new
  def new
    @transaction = Transaction.new
  end

  def show
    @transactions = Transaction.where({ :user_id => params[:id].to_i }).order(date: :asc)  
  end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = Transaction.new(transaction_params)

    respond_to do |format|
      if @transaction.save
        format.html { redirect_to @transaction, notice: 'Transaction was successfully created.' }
        format.json { render :show, status: :created, location: @transaction }
      else
        format.html { render :new }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_params
      params.require(:transaction).permit(:description, :date, :user_id)
    end
end
