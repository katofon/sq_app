class User < ApplicationRecord
	
	validates :first_name, :presence => { :message => "Нужно задать имя" }
	validates :last_name, :presence => { :message => "Нужно задать фамилию" }

	belongs_to :department
  	validates :department_id, :presence => { :message => "Нужно задать отдел" }

  	belongs_to :position
  	validates :position_id, :presence => { :message => "Нужно задать должность" }

  	validates :state, :numericality => { :greater_than_or_equal_to => 0, :less_than_or_equal_to => 1,  :message => "Неверно задан статус" }

  	validates :salary, :numericality => { :greater_than => 0,  :message => "Неверно задан размер оклада" }

	attr_accessor :is_new

end
