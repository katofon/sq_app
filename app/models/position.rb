class Position < ApplicationRecord
	validates :name, :presence => { :message => "Нужно задать имя должности" }

	@@cahed_positions = nil


	def self.get_name_by_id id
		cahed_positions[id]
	end

	def self.reset_cahed_positions
		@@cahed_positions = nil	
	end

	def self.cahed_positions
    	return @@cahed_positions if @@cahed_positions.present?
    	@@cahed_positions = Position.all.inject({}){|res, p| res[p.id] = p.name; res}
  	end
end
