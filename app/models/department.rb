class Department < ApplicationRecord
	validates :name, :presence => { :message => "Нужно задать имя отдела" }

	@@cahed_departments = nil

	def self.get_name_by_id id
		cahed_departments[id]
	end

	def self.reset_cahed_departments
		@@cahed_departments = nil	
	end

	def self.cahed_departments
    	return @@cahed_departments if @@cahed_departments.present?
    	@@cahed_departments = Department.all.inject({}){|res, p| res[p.id] = p.name; res}
  	end


end
