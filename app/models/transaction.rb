class Transaction < ApplicationRecord
	
	def self.add_transaction user_id, type_id, date = nil, new_val = nil, old_val = nil
	    
		date = Time.now.to_i if date.nil?

	    transaction = Transaction.new(
	      {
	        "date"=> date, 
	        "user_id"=> user_id, 
	        "old_value"=> old_val,
	        "new_value"=> new_val,
	        "type_id"=> type_id
	      }
	    )
	    
	    transaction.save
  	end

  	def self.user_val_description type_id, val
  		if type_id == TransactionsHelper::DEPARTMENT_TYPE
  			Department.cahed_departments[val.to_i]
  		elsif type_id == TransactionsHelper::POSITION_TYPE
  			Position.cahed_positions[val.to_i]
  		else
  			val	
  		end		
  	end


end
