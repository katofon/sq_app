module ApplicationHelper

	def self.prepare_for_select_box hash
		hash.inject([]){|res, (id, name)| res << [name, id]; res}
	end

end
