module TransactionsHelper
	ACCEPT_JOB_TYPE = 0
	FIRE_JOB_TYPE = 1
	DEPARTMENT_TYPE = 2
	POSITION_TYPE = 3
	SALARY_TYPE = 4

	DESCRIPTIONS = {
		ACCEPT_JOB_TYPE => "Принят на работу", 
		FIRE_JOB_TYPE => "Увольнение",  
		DEPARTMENT_TYPE => "Отдел",  
		POSITION_TYPE => "Должность", 
		SALARY_TYPE => "Оклад"  
	}

end
