Rails.application.routes.draw do
  resources :transactions
  resources :departments
  resources :positions
	root 'users#index'
	resources :users
	post 'users/fire' => 'users#fire'
	get 'transactions/:user_id' => 'transactions#show'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
