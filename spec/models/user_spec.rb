require "rails_helper"

describe User do
  it "User shoud have all params" do
  	params = {}
  	lambda {User.create!(params)}.should raise_error(/Нужно задать имя/)

  	params["first_name"] = "first_name1"
  	lambda {User.create!(params)}.should raise_error(/Нужно задать фамилию/)

	params["last_name"] = "last_name1"
  	lambda {User.create!(params)}.should raise_error(/Нужно задать отдел/)

  	params["department_id"] = 0
  	lambda {User.create!(params)}.should raise_error

  	department = Department.create!({"name" => "department1"})

  	params["department_id"] = department.id
  	lambda {User.create!(params)}.should raise_error(/Нужно задать должность/)

  	params["position_id"] = 0
  	lambda {User.create!(params)}.should raise_error

  	position = Position.create!({"name" => "position1"})

  	params["position_id"] = position.id
  	lambda {User.create!(params)}.should raise_error(/Неверно задан статус/)

  	params["state"] = -1
  	lambda {User.create!(params)}.should raise_error(/Неверно задан статус/)

  	params["state"] = 2
  	lambda {User.create!(params)}.should raise_error(/Неверно задан статус/)

  	params["state"] = 1
  	lambda {User.create!(params)}.should raise_error(/Неверно задан размер оклада/)

  	params["salary"] = -1
  	lambda {User.create!(params)}.should raise_error(/Неверно задан размер оклада/)

  	params["salary"] = 0
  	lambda {User.create!(params)}.should raise_error(/Неверно задан размер оклада/)

  	params["salary"] = 1
  	user = User.create!(params)

  	user.should_not be_nil
    
  end
end